%% S6_annotated_data
% load and explore data of 'getting up'  and 'lying down' of cows
%    starting from Wijbrand Ouweltjes video annotations AND Icecube data,
%    detect lying bouts 
%
%
% 

clear variables
close all
clc

%% STEP 0: set filepaths and constants
% set session and dates
init_.sesinfo = table(['ses1';'ses2'], 'VariableNames',{'sesname'});
init_.sesinfo.sesno = ['1';'2'];
init_.sesinfo.startdate = [datetime(2019,7,3,13,00,00); ...
                          datetime(2019,07,10,11,00,00)];
init_.sesinfo.enddate = [datetime(2019,7,8,10,30,00); ...
                          datetime(2019,07,15,10,10,00)];

% set filepaths and filenames
init_.filedir = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\liestand\';
init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen ' ...
                'University & Research\iAdriaens_doc\Projects\'...
                'eEllen\B4F_indTracking_cattle\B4F_data\'];
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];
init_.cubefol = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\IceQubes\dataverwerk\';

init_.filename1 = 'CheckAtt_liestand_';
init_.filename2 = 'BasicAttentions_';
init_.filename3 = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\sessioninfo.xlsx';
init_.filename4 = 'CheckTif4_liestand_';
init_.filename5 = 'Qube2cow.xlsx';
init_.filename6 = '2019_IceCube_Lbouts.xlsx';


%% STEP 1: load iceqube data and merge with cow ids

% iceqube data
opts = detectImportOptions([init_.datadir init_.filename6],'Sheet','T_Lbouts');
data_LB = readtable([init_.datadir init_.filename6],opts);

% id data 
opts = detectImportOptions([init_.cubefol init_.filename5],'Sheet','Sheet2');
data_cubeids = readtable([init_.cubefol init_.filename5],opts);
data_cubeids = data_cubeids(datenum(data_cubeids.Omdat) < datenum(2019,07,1) & ...
                            datenum(data_cubeids.Afdat) > datenum(2019,07,15),:);

% merge LevNum with iceQube data for cowID
data_cubeids.Properties.VariableNames{4} = 'cow';
data_LB = outerjoin(data_LB,data_cubeids(:,[3 4]),'Keys','LevNum',...
                                                  'MergeKeys',1);

% sort rows per cow
data_meta.IceQ = sortrows(data_LB,'cow');

% select session 1 and 2
data_meta.IceQ = data_meta.IceQ(data_meta.IceQ.Ses < 3,:);

% clear variables
clear ans data_cubeids opts data_LB

%% STEP 2: annotated data

% load annotated data
for i = 1:size(init_.sesinfo,1)      % no of sessions included
    
    % load basic attentions (annotations) - liedown
    filename = [init_.filedir init_.filename1 ...  % Check_Att - liestand
        init_.sesinfo.sesname(i,:) '.xlsx'];
    opts = detectImportOptions(filename,'Sheet','liedown');
    data_meta.(init_.sesinfo.sesname(i,:)).annliedown ...
        = readtable(filename, opts, 'Sheet','liedown');
    
    % load basic attentions (annotations bouts) - getup
    opts = detectImportOptions(filename,'Sheet','getup');
    data_meta.(init_.sesinfo.sesname(i,:)).anngetup ...
        = readtable(filename, opts, 'Sheet','getup');
    
    % load basic attentions (lying bouts) - liedown
    filename = [init_.filedir init_.filename2 ... % Basic_attentions
        init_.sesinfo.sesname(i,:) '.xlsx'];
    opts = detectImportOptions(filename,'Sheet','liedown');
    data_meta.(init_.sesinfo.sesname(i,:)).accliedown ...
        = readtable(filename, opts, 'Sheet','liedown');
    
    % load basic attentions (lying bouts) - getup
    opts = detectImportOptions(filename,'Sheet','getup');
    data_meta.(init_.sesinfo.sesname(i,:)).accgetup ...
        = readtable(filename, opts, 'Sheet','getup');
    
    % load annotation info relative timings
    filename = [init_.filedir init_.filename4 ...  % CheckTiff_liestand
        init_.sesinfo.sesname(i,:) '.xlsx'];
    opts = detectImportOptions(filename,'Sheet','liedown');
    data_meta.(init_.sesinfo.sesname(i,:)).reftiming ...
        = readtable(filename, opts, 'Sheet','liedown');
    data_meta.(init_.sesinfo.sesname(i,:)).reftiming = ...
        data_meta.(init_.sesinfo.sesname(i,:)).reftiming(:,1:6);
    
end

% sort rows
data_meta.ses1.accgetup = sortrows(data_meta.ses1.accgetup,'cow');
data_meta.ses1.accliedown = sortrows(data_meta.ses1.accliedown,'cow');
data_meta.ses2.accgetup = sortrows(data_meta.ses2.accgetup,'cow');
data_meta.ses2.accliedown = sortrows(data_meta.ses2.accliedown,'cow');

% load sessioninfo and select only ses included
data_meta.sesinfo = readtable(init_.filename3);        % read data directly
data_meta.sesinfo = data_meta.sesinfo(:,1:4);

% convert to datetime
data_meta.sesinfo.bdtime = datetime(data_meta.sesinfo.bdtime,...
    'InputFormat','yyyyMMdd HHmmss');
data_meta.sesinfo.edtime = datetime(data_meta.sesinfo.edtime,...
    'InputFormat','yyyyMMdd HHmmss');

% only retain selected sessions
data_meta.sesinfo(~contains(data_meta.sesinfo.session,...
    cellstr(init_.sesinfo.sesno)),:) = [];

% clear workspace
clear i ii opts dates fieldname filename


%% STEP 3: meta data for reference
% ---  annotated behaviour based on video by Wijbrand Ouweltjes --- 

% add time stamps to the video annotations
data_meta.sesinfo.bnumtime = datenum(data_meta.sesinfo.bdtime);
for i = 1:height(data_meta.sesinfo)
    fieldname_ = init_.sesinfo.sesname(i,:);  % fieldname session
%     reftime = data_meta.sesinfo.bnumtime(i);  % reference time start ses

    %%%%%%-------------------------TIME SYNCH---------------------%%%%%%
    % tracklab data alerts and IceQube alerts seems to be quite well
    % synchronized. However, as calculated below, the synchronization with
    % the annotated events seems to be erroneous, and for session 1, the
    % shift is approximately 11-12 minutes and for session 2 the shift is
    % 49-50 minutes. Begin of the both sessions are respectively 13:12:05
    % and 11:50:27, so 'from the hour', approximately 12 and 50 minutes. I
    % wonder whether there was a rounding error that was not documented,
    % and whether these 12:05 and 50:27 where not taken into account when
    % setting up all the excel files I use here to calculate event timing.
    % For now, I corrected the annotated timings by subtracting these from
    % the annotated timings, and i'll need to take a window into account
    % when interpreting the data.
    data_meta.sesinfo.bnumtime(i) = ...
              floor(datenum(data_meta.sesinfo.bdtime(i))) ...
            + hour(data_meta.sesinfo.bdtime(i))/24;
    %%%%%%-------------------------TIME SYNCH---------------------%%%%%%
    
    
    
    % reference datetime for lie down
    data_meta.(fieldname_).reftiming.numtime_t0 = ...
             data_meta.sesinfo.bnumtime(i) + ...   % begintime of session
             ... % no. of days since begin
             data_meta.(fieldname_).reftiming.t0 ./ (24*3600); % sec/day

    % reference datetime for get up
    data_meta.(fieldname_).reftiming.numtime_t2 = ...
             data_meta.sesinfo.bnumtime(i) + ... % begintime of session
             ... % no. of days since begin
             data_meta.(fieldname_).reftiming.t2 ./ (24*3600); % sec/day

    % add time to annotated events fb and fe = no. of frames from ref
    data_meta.(fieldname_).annliedown = ...
        innerjoin(data_meta.(fieldname_).annliedown, ...
                  ... % cow nr begin of event in numeric time down
                  data_meta.(fieldname_).reftiming(:,[1 6 7]),... 
                  'Keys',{'nr','cow'});
    data_meta.(fieldname_).annliedown  = sortrows(...
                data_meta.(fieldname_).annliedown,'cow');
    data_meta.(fieldname_).annliedown.numstart = ...
          data_meta.(fieldname_).annliedown.numtime_t0 ...  % numeric time
        + (data_meta.(fieldname_).annliedown.fb./(15*24*3600)); % sec/day
    data_meta.(fieldname_).annliedown.numend = ...
          data_meta.(fieldname_).annliedown.numtime_t0 ...
        + (data_meta.(fieldname_).annliedown.fe./(15*24*3600));
    data_meta.(fieldname_).annliedown.starttime = ...
          datetime(data_meta.(fieldname_).annliedown.numstart,...
          'ConvertFrom','datenum');
    data_meta.(fieldname_).annliedown.endtime = ...
          datetime(data_meta.(fieldname_).annliedown.numend,...
          'ConvertFrom','datenum');
    data_meta.(fieldname_).annliedown.duration = ...
          (data_meta.(fieldname_).annliedown.numend - ...
          data_meta.(fieldname_).annliedown.numstart)*24*3600; % in sec

    % getup timing
    data_meta.(fieldname_).anngetup = ...
        innerjoin(data_meta.(fieldname_).anngetup, ...
                  data_meta.(fieldname_).reftiming(:,[1 6 8]),...
                  'Keys',{'nr','cow'});
    data_meta.(fieldname_).anngetup  = sortrows(...
                data_meta.(fieldname_).anngetup,'cow');
    data_meta.(fieldname_).anngetup.numstart = ...
          data_meta.(fieldname_).anngetup.numtime_t2 ...
        + (data_meta.(fieldname_).anngetup.fb./(15*24*3600));
    data_meta.(fieldname_).anngetup.numend = ...
          data_meta.(fieldname_).anngetup.numtime_t2 ...
        + (data_meta.(fieldname_).anngetup.fe./(15*24*3600));
    data_meta.(fieldname_).anngetup.starttime = ...
          datetime(data_meta.(fieldname_).anngetup.numstart,...
          'ConvertFrom','datenum');
    data_meta.(fieldname_).anngetup.endtime = ...
          datetime(data_meta.(fieldname_).anngetup.numend,...
          'ConvertFrom','datenum');
    data_meta.(fieldname_).anngetup.duration = ...
          (data_meta.(fieldname_).anngetup.numend - ...
          data_meta.(fieldname_).anngetup.numstart)*24*3600; % in sec 
end

% put all data together in one file
data_meta.allses = [];
for i = 1:height(data_meta.sesinfo)
    fieldname_ = init_.sesinfo.sesname(i,:);  % fieldname session
    data_meta.allses = [data_meta.allses; ...
                       data_meta.(fieldname_).annliedown(:,[1 2 5 6 8:end]) ...
                       table(zeros(size(data_meta.(fieldname_).annliedown,1),1),...
                       'VariableNames',{'updown'})... % down = 0
                       table(repmat(data_meta.sesinfo.session{i,:},...
                                    size(data_meta.(fieldname_).annliedown,1),1),...
                       'VariableNames',{'sesname'});...
                       data_meta.(fieldname_).anngetup(:,[1 2 5 6 8:end]) ...
                       table(ones(size(data_meta.(fieldname_).anngetup,1),1),...
                       'VariableNames',{'updown'})... % up = 1
                       table(repmat(data_meta.sesinfo.session{i,:},...
                                    size(data_meta.(fieldname_).anngetup,1),1),...
                       'VariableNames',{'sesname'})];
end
data_meta.allses = sortrows(data_meta.allses,[1 2]);

% lying bouts
test = data_meta.allses(data_meta.allses.updown == 0,:);
test2 = data_meta.allses(data_meta.allses.updown == 1,:);
test2.Properties.VariableNames{5} = 'no_uptime';
test2.Properties.VariableNames{7} = 'uptime';
test3 = innerjoin(test,test2(:,[1 2 5 7]),'Keys',{'cow','nr'});
test3 = test3(:,[1 2 5 12 7 13 11]);
test3.Properties.VariableNames{3} = 'no_downtime';
test3.Properties.VariableNames{5} = 'downtime';
data_meta.lying = test3;
clear test3
data_meta.lying.durlydown = (data_meta.lying.no_uptime - ...
                            data_meta.lying.no_downtime)*24*60;


% add duration annotated
data_meta.IceQ.durlydown(:,1) = data_meta.IceQ.dLB.*24.*60;

% add start in IceQ data
data_meta.IceQ.numtime = datenum(data_meta.IceQ.bDat) + data_meta.IceQ.bTim;
data_meta.IceQ.time = datetime(data_meta.IceQ.numtime,'ConvertFrom','datenum');
 
data_meta.IceQ.time = datetime(data_meta.IceQ.numtime,'ConvertFrom','datenum');
% clear variables
clear i fieldname_ ans reftime opts filename

% save data_meta
save([init_.datadir 'D0_meta_events.mat'],'data_meta');


%% compare IceCube with video annotations
% unique cows
cows = unique(data_meta.allses.cow);

% figure per cow
for i = 1:length(cows)

   
    % plot data_meta.IceQ
    ind = find(data_meta.IceQ.cow == cows(i));
    
    if ~isempty(ind) && sum(data_meta.lying.cow == cows(i)) ~= 0
        % prepare figure
        h = figure('Units','centimeters','OuterPosition', [5 5 25 15]);
        hold on; box on

        plot(1000,1000,'rs','MarkerFaceColor','r','MarkerSize',8);
        plot(1000,1000,'s','MarkerFaceColor',[0 0 139]./255,'MarkerSize',8);
        legend({'IceQ','Ann'},'AutoUpdate','off')
        
        % icecube data
        startt = datenum(data_meta.IceQ.bDat(ind)) + data_meta.IceQ.bTim(ind);
        stopt = datenum(data_meta.IceQ.eDat(ind)) + data_meta.IceQ.eTim(ind);
        for ii = 1:length(startt)
            plot([startt(ii) stopt(ii)],[1 1],'r-','LineWidth',7)
        end

        % plot data_meta.lying =annotations
        ind2 = find(data_meta.lying.cow == cows(i));
        startt2 = datenum(data_meta.lying.downtime(ind2));
        stopt2 = datenum(data_meta.lying.uptime(ind2));
        for ii = 1:length(startt2)
            plot([startt2(ii) stopt2(ii)],[2 2],'-','LineWidth',7,...
                        'Color',[0 0 139]./255)
        end
        try
            xlim([-0.1+(max(min(startt),min(startt2))) min(max(stopt),max(stopt2))+0.1])
        catch
            
        end
        ylim([-5 8])
        grid on
        
        title(['Cow = ' num2str(cows(i)) ', corrected time shift'])
%         title(['Cow = ' num2str(cows(i)) ', uncorrected time shift'])
        xlabel('Numeric time')
        ylabel('Lying bouts')
%         % corresponding bouts based on starttime and length
%         durt = data_meta.IceQ.durlydown(ind);
%         durt2 = data_meta.lying.durlydown(ind2);
%         for ii = 1:length(ind)  % for all IceQube data
%             idx = find(startt2 > startt(ii) & ...   % larger than iceQ
%                        startt2 < startt(ii) + 2 & ... % smaller than iceQ+2
%                        stopt2 > stopt(ii) & ...
%                        stopt2 < stopt(ii) + 2 & ...
%                        (durt2-durt(ii)) < 0.05);
%             if length(idx) == 1
%                 data_meta.IceQ.nr(ind(ii)) = ...;
%                     data_meta.lying.nr(ind2(idx));
%             end
%         end
        
%         saveas(h,[init_.resdir 'Fig_timeshift_CORRECTED' num2str(cows(i)) '.tif'])
%         saveas(h,[init_.resdir 'Fig_timeshift_CORRECTED' num2str(cows(i)) '.fig'])
    end

end

%% STEP 3: visualisation/summary of the annotated reference data
% per animal, per session (~ flooring)
% 
%

% summary table of annotated data - per cow
cows = unique(data_meta.allses.cow);   % unique animals
data_meta.sum.cow = table(cows,'VariableNames',{'cow'});
for i = 1:length(cows)
    % indices of cow in allses
    idx = find(data_meta.allses.cow == cows(i));
    
    % summaries
    data_meta.sum.cow.first(i) = min(data_meta.allses.starttime(idx));
    data_meta.sum.cow.last(i) = max(data_meta.allses.starttime(idx));
    
    % indices of cow in allses + event = up (updown = 1)
    idx = find(data_meta.allses.cow == cows(i) & ...
               data_meta.allses.updown == 1);
    
    % duration up
    data_meta.sum.cow.number_up(i) = length(idx);
    data_meta.sum.cow.avgdur_up(i) = mean(data_meta.allses.duration(idx));
    data_meta.sum.cow.stddur_up(i) = std(data_meta.allses.duration(idx));
    data_meta.sum.cow.mindur_up(i) = min(data_meta.allses.duration(idx));
    data_meta.sum.cow.maxdur_up(i) = max(data_meta.allses.duration(idx));
    
    % indices of cow in allses + event = down (updown = 0)
    idx = find(data_meta.allses.cow == cows(i) & ...
               data_meta.allses.updown == 0);
    
    % duration up
    data_meta.sum.cow.number_down(i) = length(idx);
    data_meta.sum.cow.avgdur_down(i) = mean(data_meta.allses.duration(idx));
    data_meta.sum.cow.stddur_down(i) = std(data_meta.allses.duration(idx));
    data_meta.sum.cow.mindur_down(i) = min(data_meta.allses.duration(idx));
    data_meta.sum.cow.maxdur_down(i) = max(data_meta.allses.duration(idx));
    
    % session & dates linked with flooring !! hardcoded !!
    if max(data_meta.sum.cow.last(i)) <= datetime(2019,07,09)
        data_meta.sum.cow.ses(i,:) = 'ses1';
        data_meta.sum.cow.floor(i,:) = 'straw';     % 1 = for straw
    else
        data_meta.sum.cow.ses(i,:) = 'ses2';
        data_meta.sum.cow.floor(i,:) = 'synth';     % 2 = for synthetic
    end
end

% box plots with raw data per cow
cows = unique(data_meta.allses.cow);
fig.g = [data_meta.allses.cow, data_meta.allses.updown];
fig.lbls = [num2str(sortrows([cows;cows]))...
            repmat([', ';', '],length(cows),1)];
fig.lbls1 =  repmat(['D';'U'],length(cows),1);
fig.bpg = boxplot(data_meta.allses.duration,...
            fig.g,'PlotStyle','compact',...
            'Labels',{fig.lbls,fig.lbls1});
    t = findobj(gca,'Tag','Outliers');
        set(t,'MarkerEdgeColor',[0 0 0])
        set(t,'MarkerSize',2)
    t = findobj(gca,'Tag','Whisker');
        set(t(1:2:end),'Color','r');
    t = findobj(gca,'Tag','MedianInner');
        set(t(1:2:end),'MarkerEdgeColor','r');
    t = findobj(gca,'Tag','MedianOuter');
        set(t(1:2:end),'MarkerEdgeColor','r');
    t = findobj(gca,'Tag','Box');
        set(t(1:2:end),'Color','r');
fig.bp = gca; hold on; grid on; box on; 
set(gcf,'Units','Normalized');
set(gcf,'OuterPosition',[0.1 0.1 0.8 0.65]);
xlabel('Cow ID'); ylabel('Duration (s)'); 
title('Annotated duration of standing up and lying down per cow (all data)')
saveas(fig.bp,[init_.resdir 'Fig_annupdown_alldata.fig'])
saveas(fig.bp,[init_.resdir 'Fig_annupdown_alldata.tif'])
close all

% box plots with raw data per cow, excluding zeros
figure();
idx = find(data_meta.allses.duration > 0.5);
cows = unique(data_meta.allses.cow(idx));
fig.g = [data_meta.allses.cow(idx), data_meta.allses.updown(idx)];
fig.lbls = [num2str(sortrows([cows;cows]))...
            repmat([', ';', '],length(cows),1)];
fig.lbls1 =  repmat(['D';'U'],length(cows),1);
fig.bpg = boxplot(data_meta.allses.duration(idx),...
            fig.g,'PlotStyle','compact',...
            'Labels',{fig.lbls,fig.lbls1});
    t = findobj(gca,'Tag','Outliers');
        set(t,'MarkerEdgeColor',[0 0 0])
        set(t,'MarkerSize',2)
    t = findobj(gca,'Tag','Whisker');
        set(t(1:2:end),'Color','r');
    t = findobj(gca,'Tag','MedianInner');
        set(t(1:2:end),'MarkerEdgeColor','r');
    t = findobj(gca,'Tag','MedianOuter');
        set(t(1:2:end),'MarkerEdgeColor','r');
    t = findobj(gca,'Tag','Box');
        set(t(1:2:end),'Color','r');
fig.bpnz = gca; hold on; grid on; box on; 
set(gcf,'Units','Normalized');
set(gcf,'OuterPosition',[0.1 0.1 0.8 0.65]);
xlabel('Cow ID'); ylabel('Duration (s)');ylim([0 16.5])
title('Annotated duration of standing up and lying down per cow (nonzero data)')
saveas(fig.bpnz,[init_.resdir 'Fig_annupdown_nonzero.fig'])
saveas(fig.bpnz,[init_.resdir 'Fig_annupdown_nonzero.tif'])
close all

% box plots with raw data per flooring type
idx = find(data_meta.allses.duration > 0.5);
cows = unique(data_meta.allses.cow(idx));
fig.g = [str2num(data_meta.allses.sesname(idx)),...
         data_meta.allses.cow(idx), ...
         data_meta.allses.updown(idx)];
figure();
fig.bpg = boxplot(data_meta.allses.duration(idx),...
            fig.g,'PlotStyle','compact');
    t = findobj(gca,'Tag','Outliers');
        set(t,'MarkerEdgeColor',[0 0 0])
        set(t,'MarkerSize',2)
    t = findobj(gca,'Tag','Whisker');
        set(t(1:2:end),'Color','r');
    t = findobj(gca,'Tag','MedianInner');
        set(t(1:2:end),'MarkerEdgeColor','r');
    t = findobj(gca,'Tag','MedianOuter');
        set(t(1:2:end),'MarkerEdgeColor','r');
    t = findobj(gca,'Tag','Box');
        set(t(1:2:end),'Color','r');
fig.bp = gca; hold on; grid on; box on; ylim([0 16])
set(gcf,'Units','Normalized');
set(gcf,'OuterPosition',[0.1 0.1 0.8 0.65]);
xlabel('Cow ID'); ylabel('Duration (s)')
title('Annotated duration of standing up (1) and lying down (0) per cow')
text(1,15,'Deeplitter flooring')
text(40,15,'Synthetic flooring')
saveas(fig.bp,[init_.resdir 'Fig_annupdown_flooring.fig'])
saveas(fig.bp,[init_.resdir 'Fig_annupdown_flooring.tif'])
close all

% histogram all cows
fig.h = figure();
idx = find(data_meta.allses.updown == 0 & ...  % down
           data_meta.allses.duration > 1);
fig.h1 = histogram(data_meta.allses.duration(idx));
    fig.h1.FaceColor = [158/255 25/255 176/255];
hold on
idx = find(data_meta.allses.updown == 1 & ...  % up
           data_meta.allses.duration > 1);
fig.h2 = histogram(data_meta.allses.duration(idx));
    fig.h2.FaceColor = [30/255, 38/255, 199/255];
legend({'down','up'});
xlim([1 14]); xlabel('Duration (s)'); ylabel('No. of events');
title('Duration to get up and lie down over all cows and both floorings')
saveas(fig.h,[init_.resdir 'Fig_hist_alldata.fig'])
saveas(fig.h,[init_.resdir 'Fig_hist_alldata.tif'])


% histogram all cows
fig.h = figure();
fig.h.Units = 'centimeters';
fig.h.OuterPosition = [2 2 25 12];
idx = find(data_meta.allses.updown == 0 & ...  % down
           str2num(data_meta.allses.sesname) == 1 & ...
           data_meta.allses.duration > 1);
subplot(1,2,1); hold on; box on;
fig.h1 = histogram(data_meta.allses.duration(idx));
    fig.h1.FaceColor = [158/255 25/255 176/255];
idx = find(data_meta.allses.updown == 1 & ...  % up
           str2num(data_meta.allses.sesname) == 1 & ...
           data_meta.allses.duration > 1);
fig.h2 = histogram(data_meta.allses.duration(idx));
    fig.h2.FaceColor = [30/255, 38/255, 199/255];
legend({'down','up'});
xlim([1 14]); xlabel('Duration (s)'); ylabel('No. of events');
title('Duration to get up and lie down, deeplitter')
idx = find(data_meta.allses.updown == 0 & ...  % down
           str2num(data_meta.allses.sesname) == 2 & ...
           data_meta.allses.duration > 1);
subplot(1,2,2); hold on; box on;
fig.h1 = histogram(data_meta.allses.duration(idx));
    fig.h1.FaceColor = [158/255 25/255 176/255];
idx = find(data_meta.allses.updown == 1 & ...  % up
           str2num(data_meta.allses.sesname) == 2 & ...
           data_meta.allses.duration > 1);
fig.h2 = histogram(data_meta.allses.duration(idx));
    fig.h2.FaceColor = [30/255, 38/255, 199/255];
legend({'down','up'});
xlim([1 14]); xlabel('Duration (s)'); ylabel('No. of events');
title('Duration to get up and lie down, synthetic')
saveas(fig.h,[init_.resdir 'Fig_hist_flooring.fig'])
saveas(fig.h,[init_.resdir 'Fig_hist_flooring.tif'])


% link time up and time down? Via difference plotting
test = data_meta.allses(data_meta.allses.duration > 1, [1 2 7 9 10 11]);% select data needed
[~,b] = unique(test(:,[1 2]),'rows');       % find unique cow/nr
b(1:end-1,2) = diff(b(:,1));                % calculate difference indices
b(b(:,2) ~= 2,:) = [];                      % if difference not 2 delete
b = table(sortrows([b(:,1);b(:,1)+1]),'VariableNames',{'index'});
test.index = (1:height(test))';             % add index to test
test = innerjoin(test,b);                   % join = delete when ~index
test.difdur(:,1) = NaN;                     % add difference in duration
test.difdur(2:end,1) = diff(test.duration);
test.difdur(1:2:end,1) = NaN;
fig.h = figure();       
fig.h.Units = 'centimeters';
fig.h.OuterPosition = [2 2 25 12];
idx = find(~isnan(test.difdur) & ...  % not nan
           str2num(test.sesname) == 1);
subplot(1,2,1); hold on; box on;
title('Difference duration at cow level, deeplitter')
fig.h1 = histogram(test.difdur(idx));
    fig.h1.FaceColor = [23/255 232/255 34/255];
xlim([-8 11]); xlabel('Duration difference (s)'); ylabel('No. of events');
subplot(1,2,2); hold on; box on;
title('Difference duration at cow level, synthetic')
idx = find(~isnan(test.difdur) & ...  % not nan
           str2num(test.sesname) == 2);
fig.h2 = histogram(data_meta.allses.duration(idx));
    fig.h2.FaceColor = [23/255 232/255 34/255];
xlim([-8 11]); xlabel('Duration difference (s)'); ylabel('No. of events');
saveas(fig.h,[init_.resdir 'Fig_hist_diffdur_flooring.fig'])
saveas(fig.h,[init_.resdir 'Fig_hist_diffdur_flooring.tif'])
close all

% clear variables
clear a ans b cows fig i idx t test y

% ----------------- some remarks on the figures ----------------- %
% there seems to be some cow individuality and trends, both depending on
% the flooring and on the cow. A repeated measures ANOVA can be used to
% test significance of these differences, and could be a separate result as
% well, for example in the context of the KB DDHT SP analysis. 
% the relative difference between getting up and lying down can be due to
% the 'rules' for annotation and doesn't mean lying down indeed lasts
% shorter than getting up. However, it should be consistent and the 
% flooring fig suggest mainly longer duration for 'up' in synthetic floors.
% the variability in both traits seems to be comparabel, and the distribution
% as well (right-skewed) with more variance in synthetic floor.
% ----------------- some remarks on the figures ----------------- %


%% Summary per cow & FIG : No. of events per day and duration of lying down per day







