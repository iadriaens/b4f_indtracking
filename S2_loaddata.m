%% S2_dataexpl
% This script generates results for the data exploration and visualisation
% of the UWB positioning data. The script is divided in different sections,
% all focusing on different approaches of the data analysis
% 
% STEP0: settings, filepaths and constants
% STEP1: load data
% STEP2: averages and variability - cleaning, editing and smoothing
% STEP3: missing data
% STEP4: interpretation of location and distances (x,y)
% STEP5: interpratation of location (z)
% functions (e.g. to generate 'background' barn map + zones
clear variables
close all
clc

%% STEP0: settings, filepaths and constants
% set path where to read files
paths.datadir = ['C:\Users\adria036\OneDrive - Wageningen University & '...
           'Research\iAdriaens_doc\Projects\eEllen\'...
           'B4F_indTracking_cattle\B4F_results\'];

% set path where results need to be stored
paths.resdir = paths.datadir;
       
% no of seconds accumulated => currently per 5s (in filename)
cte.nseconds = 5;

% detect files accumulated per nseconds
paths.files = ls([paths.datadir 'trk*' num2str(cte.nseconds) 'sec.txt']); 

% file temperature
paths.dircopy = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_data\'];
paths.fn = ls([paths.dircopy '*temperature.xlsx']);

% file cow ancillary information
paths.fnancil = 'D_cowAncInfo.xlsx'; % cowinfo

% dates (incl) in which data need to be read
cte.startdate = datetime(2019,7,1);
cte.enddate = datetime(2019,7,31);

%% STEP1: load and select data

% read data
tic
for i = 1:size(paths.files,1)
    disp(['Current file reading = ' paths.files(i,:)])   
    
    % set fieldname
    split = regexp(paths.files(i,:),'_','split');
    field = ['d' split{1,2}];
   
    % load data if between startdate and enddate
    if (datetime(split{1,2},'InputFormat','yyyyMMdd') >= cte.startdate ...
            && datetime(split{1,2},'InputFormat','yyyyMMdd') <= cte.enddate) 
        
        % detect import options > accelerates the reading process
        opts = detectImportOptions([paths.datadir paths.files(i,:)]);

        % read data in struct array
        data.(field) = readtable([paths.datadir paths.files(i,:)],opts);
    end
end
toc

% read temperature data
opts = detectImportOptions([paths.dircopy paths.fn]);
opts = setvartype(opts,4:8,'double');

% read table with weather data
weatherData = readtable([paths.dircopy paths.fn],opts);

% read ancillary cow info
cowinfo = readtable([paths.dircopy paths.fnancil]);
cowinfo = innerjoin(cowinfo,unique(data.d20190701(:,1)),...
                     'LeftKeys','OorNum',...
                     'RightKeys','object_name');


% clear variables
clear opts split field i

%% plot cowinfo
% cowids
cowids = unique(cowinfo.OorNum);

% visualise daily milkproduction
close all
h = figure('Units','centimeters','OuterPosition',[1 1 30 15]);
    hold on; box on;
    xlabel('Date');
    ylabel('Milk yield (kg/d)')
cols = F0_othercolor('Mrusttones',length(cowids));  % set colors
colsgr = F0_othercolor('Greys9',2*length(cowids));  % set colors
for i = 1:length(cowids)
   
    % find data from cowid in entire dataset
    ind = find(cowinfo.OorNum == cowids(i) & ~isnan(cowinfo.DMY) &...
               (datenum(cowinfo.Dat) > datenum(2019,5,1) & ...
                datenum(cowinfo.Dat) < datenum(2019,9,1)));
    plot([cowinfo.Dat(ind(1)) cowinfo.Dat(ind(1))],[0 60],'--','Color',cols(i,:),...
         'LineWidth',1)
     
    % plot the entire milking data per date
    plot(cowinfo.Dat(ind),cowinfo.DMY(ind),'o-','Color',colsgr(i,:),...
         'LineWidth',0.7,'MarkerSize',3)
    
    % find data from cowid in entire dataset
    ind = find(cowinfo.OorNum == cowids(i) & ~isnan(cowinfo.DMY) &...
               (datenum(cowinfo.Dat) > datenum(2019,6,30) & ...
                datenum(cowinfo.Dat) < datenum(2019,8,1)));
    
    % plot the milking data in july
    plot(cowinfo.Dat(ind),cowinfo.DMY(ind),':','Color',cols(i,:),...
         'LineWidth',1.5)

    
end
    
axis tight
title('Overview milk yield (kg/d) cows in tracklab trial')

% save figure
dirresults = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_results\'];
saveas(h,[dirresults 'FIG_DMYallcows.fig']);
saveas(h,[dirresults 'FIG_DMYallcows.png']);

% clear variables
clear ind h i cols colsgr dirresults cowids


