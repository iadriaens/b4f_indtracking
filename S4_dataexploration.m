%% S4_dataexploration
% this scripts explores the x, y and z data in function of time and
% multiple other factors

% clear workspace
clear variables
close all
clc
% load data > data, weatherData, cowinfo
tic
S2_loaddata
toc

% set results path
paths.resdir = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_results\'];

%% STEP2: averages and variability + basic visualization function

% fields in data
fields_ = fieldnames(data);

% summarize and plot position in function of time
close all
h = [];
for i = 1:size(fields_,1)
        
    % calculate summary stats per day
    summary.day.(fields_{i}) = groupsummary(data.(fields_{i}),"object_name",...
             ["mean","std","nummissing","nnz"],["avg_x","avg_y","avg_z"]);
    
    % set plot color map
    plt.cols = autumn(size(fields_,1));
         
    % plot average cow position per day
    for j = 1:height(summary.day.(fields_{i}))
        % figure handle > make sure the same ID is plotted on the same fig
        plt.cowfighandle = sprintf('cow_%d',summary.day.(fields_{i}).object_name(j)); 
        % if a fig exists > use fig; else create new fig
        if ~isfield(h,plt.cowfighandle)
            h.(plt.cowfighandle) = makebarnfig(30);
            title(['object\_name = cow ', ...
                   num2str(summary.day.(fields_{i}).object_name(j)), ...
                   ', dot = avg position, circle proportional to variablity'],...
                   'Color',[0.7 0.7 0.7])
        else
            figure(h.(plt.cowfighandle))
        end
        % plot evg cow position, color = day of the month
        plot(summary.day.(fields_{i}).mean_avg_x(j),...
             summary.day.(fields_{i}).mean_avg_y(j),...
             'o','MarkerSize',6,'MarkerFaceColor',plt.cols(i,:),...
             'MarkerEdgeColor',plt.cols(i,:))
        
        % if less than 20% data for a certain day, plot black x on top
        if summary.day.(fields_{i}).nnz_avg_x(j) < 0.2*summary.day.(fields_{i}).GroupCount(j)
            plot(summary.day.(fields_{i}).mean_avg_x(j),...
                 summary.day.(fields_{i}).mean_avg_y(j),...
                 'kx','MarkerSize',10,'LineWidth',2)      
        else % circle =  variability in position over that day
            plt.vari = 0.01 + 2*summary.day.(fields_{i}).std_avg_x(j)...
                            + 2*summary.day.(fields_{i}).std_avg_y(j);
            plot(summary.day.(fields_{i}).mean_avg_x(j),...
                 summary.day.(fields_{i}).mean_avg_y(j),...
                 'o','MarkerSize',plt.vari,...
                 'MarkerEdgeColor',plt.cols(i,:),'LineWidth',1)
        end
        
        % save figures in resdir if i = end of array
        if i == size(fields_,1)
            saveas(h.(plt.cowfighandle),[paths.resdir 'FIG_posTime_' ...
                        num2str(summary.day.(fields_{i}).object_name(j)) '.png']) 
        end
    end 
end


% plot summary results
% one figure per day, all cows in the barn




%% frequency of cow location
close all
% fields
fields_ = fieldnames(data);

h = [];
for i = 2:size(fields_,1)
    
    % round x and y data on 50 cm (=1/2m)
    data.(fields_{i}).ravg_x(:,1) = round(data.(fields_{i}).avg_x(:,1)*2)/2;
    data.(fields_{i}).ravg_y(:,1) = round(data.(fields_{i}).avg_y(:,1)*2)/2;
    
    
    % cows in data
    cows = unique(data.(fields_{i}).object_name);
    
    for j = 1:length(cows)
        % figure handle > make sure the same ID is plotted on the same fig
        plt.cowfighandle = sprintf('cow_%d',cows(j));
        
        % if a fig exists > use fig; else create new fig
        if ~isfield(h,plt.cowfighandle)
            h.(plt.cowfighandle) = figure('Units','centimeters');
            h.(plt.cowfighandle).OuterPosition = [1 1 37 22];
        else
            figure(h.(plt.cowfighandle))
        end

        subplot(5,6,i-1);  % day
        ind = find(data.(fields_{i}).object_name == cows(j));
        
        hi = heatmap(data.(fields_{i})(ind,:),'ravg_x','ravg_y',...
                     'ColorMethod','count',...
                     'GridVisible',0,...
                     'FontSize',8,...
                     'ColorbarVisible','off',...
                     'Title',[num2str(cows(j)) ', ' fields_{i}]) ;
        ax = gca;
        ax.XDisplayLabels = nan(size(ax.XDisplayData));
        ax.YDisplayLabels = nan(size(ax.YDisplayData));
        
        if i == size(fields_,1)
            saveas(h.(plt.cowfighandle),[paths.resdir 'FIG_heatmapPosition_' num2str(cows(j))])
        end
    end
end

figfield = fields(h);
for j = 1:size(fields(h))
    % figure handle > make sure the same ID is plotted on the same fig
    plt.cowfighandle = figfield{j};

    % figure
     figure(h.(plt.cowfighandle))

    % save
    saveas(h.(plt.cowfighandle), [paths.resdir 'FIG_heatmapPosition_' num2str(cows(j)) '.png'])

end










%% Average position in barn all day (24h)
% fields in data
fields_ = fieldnames(data);


% summarize and plot position in function of time
close all
h = [];
for i = 1:size(fields_,1)
        
    % calculate summary stats per day
    summary.day.(fields_{i}) = groupsummary(data.(fields_{i}),"object_name",...
             ["mean","std","nummissing","nnz"],["avg_x","avg_y","avg_z"]);
    
    % set plot color map
    plt.cols = autumn(size(fields_,1));
         
    % plot average cow position per day
    for j = 1:height(summary.day.(fields_{i}))
        % figure handle > make sure the same ID is plotted on the same fig
        plt.cowfighandle = sprintf('cow_%d',summary.day.(fields_{i}).object_name(j)); 
        % if a fig exists > use fig; else create new fig
        if ~isfield(h,plt.cowfighandle)
            h.(plt.cowfighandle) = makebarnfig(30);
            title(['object\_name = cow ', ...
                   num2str(summary.day.(fields_{i}).object_name(j)), ...
                   ', dot = avg position, circle proportional to variablity'],...
                   'Color',[0.7 0.7 0.7])
        else
            figure(h.(plt.cowfighandle))
        end
        % plot evg cow position, color = day of the month
        plot(summary.day.(fields_{i}).mean_avg_x(j),...
             summary.day.(fields_{i}).mean_avg_y(j),...
             'o','MarkerSize',6,'MarkerFaceColor',plt.cols(i,:),...
             'MarkerEdgeColor',plt.cols(i,:))
        
        % if less than 20% data for a certain day, plot black x on top
        if summary.day.(fields_{i}).nnz_avg_x(j) < 0.2*summary.day.(fields_{i}).GroupCount(j)
            plot(summary.day.(fields_{i}).mean_avg_x(j),...
                 summary.day.(fields_{i}).mean_avg_y(j),...
                 'kx','MarkerSize',10,'LineWidth',2)      
        else % circle =  variability in position over that day
            plt.vari = 0.01 + 2*summary.day.(fields_{i}).std_avg_x(j)...
                            + 2*summary.day.(fields_{i}).std_avg_y(j);
            plot(summary.day.(fields_{i}).mean_avg_x(j),...
                 summary.day.(fields_{i}).mean_avg_y(j),...
                 'o','MarkerSize',plt.vari,...
                 'MarkerEdgeColor',plt.cols(i,:),'LineWidth',1)
        end
        
        % save figures in resdir if i = end of array
        if i == size(fields_,1)
            saveas(h.(plt.cowfighandle),[paths.resdir 'FIG_posTime_' ...
                        num2str(summary.day.(fields_{i}).object_name(j)) '.png']) 
        end
    end 
end

%% Average position in barn afternoon between milkings 11:00 to 16:00

%% Average position in barn morning to evening


% conclusion: needs filtering
% drift? on UWP positioning or is it cow behaviour we see? > try min max
% correction per day!!!

% per day range of position
% figure: 

%% visualization barn position slatted vs lying area
% per day when at feeding rack = y > 11.5
% write it in for loop with subplots
close all
clear hi
Tfig = 0; % counter for save name
for i = 1:size(fields_,1)
    if rem(i,4) == 1 % start new figure if remainder of i = 1
        h = figure('Units','centimeters','OuterPosition',[1 1 35 20]);
        T = 1;
        Tfig = Tfig+1;
    end
    % plot 4 days per figure
    subplot(2,2,T); hold on; box on;
    field_ = fields_{i};
    xlabel('Time of the day (hours)'); xlim([0 24])
    ylabel('Proportion of time per area [%]'); ylim([0 0.03])
    title(['Date = ' field_])
    % find all data with y > 11 and make histogram
    ind = find(data.(field_).avg_y > 11); % find cows in slatted floor regio
        hi.slat = histogram(data.(field_).timeH(ind),24*4,'Normalization','probability');
        hi.slat.FaceColor = [13/255 48/155 189/255];
    ind = find(data.(field_).avg_y < 10); % find cows in liestall regio
        hi.lie = histogram(data.(field_).timeH(ind),24*4,'Normalization','probability');
        hi.lie.FaceColor = [255/255 102/255 0/255];
    legend({'slatted','lie'},'AutoUpdate','off','Location','northwest')
    % plot approximate milking times
    plot([6 6],[0 0.05],'r-.','LineWidth',1.5) % milking AM
    plot([8 8],[0 0.05],'r-.','LineWidth',1.5) % milking AM
    plot([16 16],[0 0.05],'r-.','LineWidth',1.5) % milking PM
    plot([18 18],[0 0.05],'r-.','LineWidth',1.5) % milking PM
    hold off
    if T == 4 || i == size(fields_,1)
        saveas(h,[paths.resdir 'FIG_AreaHist_' ...
                        num2str(Tfig) '.png']) 
    end
    T=T+1;    
end

% test
test = 0:0.25:24;



%% visualization position afternoon vs heat stress
% summarize and plot position in function of temperature
%   THI max = THI at max per day
%   interesting = if on hottest moment of the day e.g. 11-16h
close all
h = [];
for i = 1:size(fields_,1)
        
    % summary of PM data 11-16
    summary.pm1116.(fields_{i}) = groupsummary(data.(fields_{i})(...
             data.(fields_{i}).timeH >= 11 &...
             data.(fields_{i}).timeH <=16,:),"object_name",...
             ["mean","std","nummissing","nnz"],["avg_x","avg_y","avg_z"]);
    
    % define colors based on THImax 'comfort zones'
    plt.cols = jet(5); % 5 comfort zones [0;70;75;80;85;100]
    plt.zones = [0 70; 70 75; 75 80; 80 85; 85 100];     
    
    % find date in weatherData
    date = fields_{i};
    idx = find(datetime(date(2:end),'InputFormat','yyyyMMdd') == ...
               datetime(weatherData.year_,weatherData.month_,weatherData.day_));
    
    % set color to plot based on THImax
    colindex = plt.cols(weatherData.THImax(idx) >= plt.zones(:,1) &...
                        weatherData.THImax(idx) < plt.zones(:,2),:);
    
    % plot position in function of heat stress zones
    for j = 1:height(summary.pm1116.(fields_{i}))
        plt.cowfighandle = sprintf('cow_%d',summary.pm1116.(fields_{i}).object_name(j)); 
        if ~isfield(h,plt.cowfighandle)
            h.(plt.cowfighandle) = makebarnfig(30);
            title(['11-16 PM; object\_name = cow ', ...
                   num2str(summary.pm1116.(fields_{i}).object_name(j)), ...
                   ', dot = avg position, circle proportional to variablity',...
                   ', col = THI class'],...
                   'Color',[0.7 0.7 0.7])
        else
            figure(h.(plt.cowfighandle))
        end
        
        plot(summary.pm1116.(fields_{i}).mean_avg_x(j),...
             summary.pm1116.(fields_{i}).mean_avg_y(j),...
             'o','MarkerSize',6,'MarkerFaceColor',colindex,...
             'MarkerEdgeColor',colindex)
        
        % if less than 20% data for a certain pm1116, plot black x on top
        if summary.pm1116.(fields_{i}).nnz_avg_x(j) < 0.2*summary.pm1116.(fields_{i}).GroupCount(j)
            plot(summary.pm1116.(fields_{i}).mean_avg_x(j),...
                 summary.pm1116.(fields_{i}).mean_avg_y(j),...
                 'kx','MarkerSize',10,'LineWidth',2)      
        else
            plt.vari = 0.01 + 2*summary.pm1116.(fields_{i}).std_avg_x(j)...
                            + 2*summary.pm1116.(fields_{i}).std_avg_y(j);
            plot(summary.pm1116.(fields_{i}).mean_avg_x(j),...
                 summary.pm1116.(fields_{i}).mean_avg_y(j),...
                 'o','MarkerSize',plt.vari,...
                 'MarkerEdgeColor',colindex,'LineWidth',1)
        end
        
        % save figures in resdir if i = end of array
        if i == size(fields_,1)
            saveas(h.(plt.cowfighandle),[paths.resdir 'FIG_posHeatPM1116_' ...
                        num2str(summary.pm1116.(fields_{i}).object_name(j)) '.png']) 
        end
    end 
end


%% visualization of z-data
close all
clc
% fields of data (dates)
fields_ = fieldnames(data);

% histogram of z data
close all
clear hi
Tfig = 0; % counter for save name
summary.z_out_bounds = table(fields_,'VariableNames',{'Date'});
for i = 1:size(fields_,1)
    if rem(i,4) == 1 % start new figure if remainder of i = 1
        h = figure('Units','centimeters','OuterPosition',[1 1 35 20]);
        T = 1;
        Tfig = Tfig+1;
    end
    % plot 4 days per figure
    subplot(2,2,T); hold on; box on;
    field_ = fields_{i};
    xlabel('z-value (m)'); xlim([0 2.5])
    ylabel('Proportion of values [%]'); ylim([0 0.018])
    title(['Date = ' field_])
    % find all data with y > 11 and make histogram
    hi = histogram(data.(field_).avg_z,100,'Normalization','probability');
        hi.FaceColor = [0/255 0/255 105/255];
    
    % calculate mode and median of bins
    M = mode(round(data.(field_).avg_z(data.(field_).avg_z>0.01 & data.(field_).avg_z<2.5),2));
    m = nanmedian(round(data.(field_).avg_z(data.(field_).avg_z>0.01 & data.(field_).avg_z<2.5),2));
    % plot
    plot([M M], [ 0 1],'r:','LineWidth',2)
    plot([m m], [ 0 1],'g:','LineWidth',2)
    legend({'data','mode','med'})
    
    % No. out of bounds
    summary.z_out_bounds.NoMissing(i) = sum(isnan(data.(field_).avg_z));
    summary.z_out_bounds.NoLow0025(i) = sum(data.(field_).avg_z<=0.025);
    summary.z_out_bounds.NoHigh245(i) = sum(data.(field_).avg_z>2.45);
    summary.z_out_bounds.NoRecords(i) = size(data.(field_),1);
%     if T == 4 || i == size(fields_,1)
%         saveas(h,[paths.resdir 'FIG_ZaverageHist_' ...
%                         num2str(Tfig) '.png']) 
%     end
    T=T+1;    
end

% calculate percentages
summary.z_out_bounds.percmiss = summary.z_out_bounds.NoMissing./summary.z_out_bounds.NoRecords*100;
summary.z_out_bounds.perclow = summary.z_out_bounds.NoLow0025./summary.z_out_bounds.NoRecords*100;
summary.z_out_bounds.perchigh = summary.z_out_bounds.NoHigh245./summary.z_out_bounds.NoRecords*100;

% display averages
disp(['On average deleted = ' num2str(mean(summary.z_out_bounds.perclow)+mean(summary.z_out_bounds.perchigh))])
disp(['On average missing = ' num2str(mean(summary.z_out_bounds.percmiss))])

%%
% calculate euclidian distances of (x,y)
% (calculate variability in distances )
% and relate that to z value
% sqrt
close all
clc
% fields of data (dates)
fields_ = fieldnames(data);


for i = 51:size(fields_,1)  % per day
    % prepare insertion
    data.(fields_{i}).timediff(:,1) = NaN;
    data.(fields_{i}).dist(:,1) = NaN;  % distance
    data.(fields_{i}).distpersec(:,1) = NaN;
    data.(fields_{i}).distmediansmooth(:,1) = NaN;
    data.(fields_{i}).avg_zsmooth(:,1) = NaN;

    % find unique cows
    cows = unique(data.(fields_{i}).object_name);

    for j = 1:length(cows)
        % indices of cow j
        idx = find(~isnan(data.(fields_{i}).avg_x) & ...
                          data.(fields_{i}).object_name == cows(j));
        set = data.(fields_{i})(idx,:);
                            
        % calculate time difference
        set.timediff(2:end,:) = diff(set.accumsubs)*5;
        
        % if time diff < 60s > distance
        ind = find(set.timediff <= 60);
        set.dist(ind) = sqrt( (set.avg_x(ind)-set.avg_x(ind-1)).^2 + ...
                         (set.avg_x(ind)-set.avg_x(ind-1)).^2);
        set.distpersec(ind) = set.dist(ind)./set.timediff(ind);
        
        % limited number distance > 0.5m/s > set to 0.5
        set.distpersec(set.distpersec > 0.5) = 0.5;  
        
% %         % enter back in data
% %         data.(fields_{i}).timediff(idx) = set.timediff;
% %         data.(fields_{i}).dist(idx) = set.dist;
% %         data.(fields_{i}).distpersec(idx) = set.distpersec;
% %         
        % transformation
        data.(fields_{i}).timediff(idx) = set.timediff;
        [data.(fields_{i}).dist(idx)] = log(set.dist+0.001);
        [data.(fields_{i}).distpersec(idx)] = log(set.distpersec+0.001);
        
        
        % smooth distances
        idx = find(data.(fields_{i}).object_name == cows(j)); % all values, also missing
        data.(fields_{i}).distmediansmooth(idx) = movmedian(data.(fields_{i}).distpersec(idx),24,'omitnan');
    
        % smooth z data
        data.(fields_{i}).avg_zsmooth(idx) = movmedian(data.(fields_{i}).avg_z(idx),24,'omitnan');
        
    end
    
    % plot for i == cows(i)
%     set = data.(fields_{i})(~isnan(data.(fields_{i}).avg_x) & ...
%                             data.(fields_{i}).object_name == cows(i),:);
     set = data.(fields_{i})(idx,:);
        h = figure('Units','centimeters','OuterPosition',[2.5 5 33 14]); hold on; box on
        plot(set.date,set.distpersec,'Color',[0.7 0.7 0.7])
        plot(set.date,set.distmediansmooth,'Color',[153/255 0 76/255],'LineWidth',1.5)
        plot(set.date, 0.05*ones(1,length(set.date)),'k--','LineWidth',0.7)
        xlabel('Time [h]')
        ylabel('Distance [m/s]')
        axis tight
        ylim([0.7 1.0])
        yyaxis right
        plot(set.date, set.avg_z,'-','Color',[204/255 229/255 255/255],'LineWidth',0.5)
        plot(set.date, set.avg_zsmooth,'-','Color',[0 0 204/255],'LineWidth',1.5)
        ylim([-2.5 2.5] )
        plot(set.date, zeros(1,length(set.date)),'k-')
        plot(set.date, ones(1,length(set.date)),'k--','LineWidth',0.7)
        ylabel('Height (z) [m]')
        if nanmean(set.avg_x) < 23
            title([fields_{i}, ', cow ' num2str(cows(i)), ', synthetic'])
        else
                        title([fields_{i}, ', cow ' num2str(cows(i)), ', deep litter'])
        end
%     saveas(h,[paths.resdir 'distance_zaxis' num2str(cows(i)) '.fig'])
%     saveas(h,[paths.resdir 'distance_zaxis' num2str(cows(i)) '.png'])
end

close all


% correlation plots per cow between median distance and median z value
close all
clear h
h = []
for i = 4:20  % per day
    % find unique cows
    cows = unique(data.(fields_{i}).object_name);
        % summary of PM data 11-16
    summary.dist_z.(fields_{i}) = table(unique(data.(fields_{i}).object_name),'VariableNames',{'object_name'});

    for j = 1:length(cows)
        % indices of cow j
        idx = find(~isnan(data.(fields_{i}).avg_x) & ...
                          data.(fields_{i}).object_name == cows(j));
        set = data.(fields_{i})(idx,:);
                            
        plt.cowfighandle = sprintf('cow_%d',summary.dist_z.(fields_{i}).object_name(j)); 
        if ~isfield(h,plt.cowfighandle)
            h.(plt.cowfighandle) = figure('Units','centimeters','OuterPosition',[1 1 35 23]);  
        else
            figure(h.(plt.cowfighandle))
        end
        
        subplot(4,4,i-3); 
        plot(set.distmediansmooth, set.avg_zsmooth, 'bo','MarkerSize',3)
        box on; xlabel('distance [m/s]'); ylabel('z (m)')
        title(['Cow ', ...
                num2str(summary.dist_z.(fields_{i}).object_name(j)), ...
                ', ' fields_{i}])
            xlim([-0.02 0.15])
            ylim([0 2.5])
    end
end




%% standing + lying down
% 1) find moments a cow remains in the same por





%% STEP4: interpretation of location data

% calculate distance per animal per day
fields_ = fieldnames(data);
% prepare list of cow ids
cowlist = unique(data.(fields_{1}).object_name);
for i = 1:size(fields_,1)
    
    data;
    summary = groupsummary(data.(fields_{i}),"object_name",...
             ["mean","std"],["location_x","location_y","location_z"]);
    
end




for i = 1:30
    h = makebarnfig(30);
end


%% functions

% ==================== MAKEBARNFIG ======================= %
% Figure of barn based on a) figure "Plattegrond Meetstal.pdf" and
% b) Wijbrand's explanation > basic design is stored in PPT_barn_axes.ppt
function h = makebarnfig(figsize)
% create a figure with input = figsize, that has barn as axes and positions
% the data such that we can interpret cow positions
% figsize is a measure in centimeters that indicates the width (=x-axis)
% and corresponds to the 43m width of the barn
% the corresponding height = 14m and the rule of three gives us that the
% height of the figure should be 14*figsize/43
% outersize is fixed on innersize + 2*1.5cm
% 
% try
%     close h
% catch
% end
    innerpos = [1+1.5 1+1.5 figsize 14/43*figsize];
    outerpos = [1 1 figsize+3 14/43*figsize+3];

    h = figure('Units','centimeters',...
               'OuterPosition',outerpos,...
               'InnerPosition',innerpos,...
               'Color',[0.1 0.1 0.1]...
               );
    grid on; hold on;
    plot([0 43],[0 0],'Color','b','LineWidth',1.5)
    plot([0 0],[0 14],'Color','b','LineWidth',1.5)
    plot(43,0,'>','MarkerSize',6,'Color','b','MarkerFaceColor','b')
    plot(0,14,'^','MarkerSize',6,'Color','b','MarkerFaceColor','b')
    axis tight
    box on
    ax = gca;
    ax.Units = 'centimeters';
    text(-0.2, -0.6,'(x,y)=(0,0)','Color',[0.7 0.7 0.7]) 
    xlabel('x position','Color',[0.7 0.7 0.7])
    ylabel('y position','Color',[0.7 0.7 0.7])
    
    % plot barn zones
    plot([21.6 21.6],[0 14],'k','LineWidth',2)
    plot([0 43],[14-3.5 14-3.5],'Color',[2/255 156/255 121/255],'LineWidth',2)
    plot([0 43],[14 14],'r-.','LineWidth',1)
end

% ==================== CALCULATE DISTANCES ======================= %