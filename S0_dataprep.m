%% Data preparation tracklab data
% this script prepares data for reading and analysis
%
% STEP1: copy .trk (= ascii text files exported from tracklab software) 
%             from the W:\ drive to a local location
% STEP2: put together weather data file and and store



%% STEP1: copy files from W:\ to local data file with command

% directory with .trk exported data files
dirTL = 'W:\ASG\WLR_Dataopslag\DWZ\1519\DC2019\tracklab_trk\';  % tracklab data directory
dircopy = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_data\'];

% list data files
allfiles = ls([dirTL '*.trk']);  % 120 days

% number of files to copy
numfiles = 36;

% copy numfiles to local folder
for i = 1:numfiles
    tic       % track time needed to copy one file
    disp(['File No. = ' num2str(i)])

    copyfile = ['"' dirTL allfiles(i,:) '"']; % file to copy
    copydir = ['"' dircopy allfiles(i,:) '"']; % destination 
    
    command = ['copy /D ' copyfile ' /A ' copydir ' /A'];  % command to copy files

    [status,cmdout] = system(command,'-echo');  % excecute command
    
    timerVal = toc;  % track time needed to copy
    disp(['Time needed to copy ' num2str(timerVal)])
end


%% STEP2: select and load data based on weather 
% set directory and filename
dircopy = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_data\'];
fn = ls([dircopy '*.xlsx']);

% detect import options
exist([dircopy fn])
opts = detectImportOptions([dircopy fn]);
opts = setvartype(opts,4:6,'double');

% read table with weather data
weatherData = readtable([dircopy fn],opts);

% plot data
close all
h = figure; hold on; grid on; box on
plot(datetime(weatherData.year,weatherData.month,weatherData.day),weatherData.mean,'Color',[0.35 0.60 0.15],'LineWidth',2);
plot(datetime(weatherData.year,weatherData.month,weatherData.day),weatherData.min,'b:','LineWidth',1.8);
plot(datetime(weatherData.year,weatherData.month,weatherData.day),weatherData.max,'r:','LineWidth',1.8);
set(gca,'FontName','Times','FontSize',12)
xlabel('Date');
ylabel('Temperature (�C)')
title('Temperature in Leeuwarden, July 2019')
legend({'avg T�','min T�','max T�'},'AutoUpdate','off','Location','northwest')
% set for plot frames
    pl.x1 = datetime(2019,07,14);
    pl.x2 = datetime(2019,07,16);
    pl.x3 = datetime(2019,07,24);
    pl.x4 = datetime(2019,07,26);
    pl.y1 = 20;
    pl.y2 = 10;
    pl.y3 = 36;
    pl.y4 = 14;
pl.Col1 = [0.929 0.694 0.125];
pl.Col2 = [0.494 0.184 0.556];
plot([pl.x1 pl.x1],[pl.y1,pl.y2],'-.','LineWidth',2,'Color',pl.Col1)
plot([pl.x1 pl.x2],[pl.y1,pl.y1],'-.','LineWidth',2,'Color',pl.Col1)
plot([pl.x1 pl.x2],[pl.y2,pl.y2],'-.','LineWidth',2,'Color',pl.Col1)
plot([pl.x2 pl.x2],[pl.y1,pl.y2],'-.','LineWidth',2,'Color',pl.Col1)
plot([pl.x3 pl.x3],[pl.y3,pl.y4],'-.','LineWidth',2,'Color',pl.Col2)
plot([pl.x3 pl.x4],[pl.y3,pl.y3],'-.','LineWidth',2,'Color',pl.Col2)
plot([pl.x3 pl.x4],[pl.y4,pl.y4],'-.','LineWidth',2,'Color',pl.Col2)
plot([pl.x4 pl.x4],[pl.y3,pl.y4],'-.','LineWidth',2,'Color',pl.Col2)



% save figure
dirresults = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_results\'];
saveas(h,[dirresults 'TempJuly.fig']);
saveas(h,[dirresults 'TempJuly.png']);

% interesting would be to compare 14-15-16 july with 24-25-26 july


%% STEP 3: Ancillary cow information - selection and visualisation

% directory
dataDir = ['C:\Users\adria036\OneDrive - Wageningen University & Research\'...
          'iAdriaens_doc\Projects\eEllen\B4F_indTracking_cattle\B4F_data\'];
      
% filename
fn = '2019_ancCowInfo.xlsx';

% detect import options
opts = detectImportOptions([dataDir fn]);

% retain variables
varnames = {'Dat','Hok','OorNum','LftDgn','KlfDat','LacDgn',...
            'InsDat','VlgKlfDat','VrwDgn',...
            'KgMlkO','KgMlkA','mpr_week_SCC'};
opts.SelectedVariableNames = varnames;

% import table
cowinfo = readtable([dataDir fn], opts);

% sort per cow and per date
cowinfo = sortrows(cowinfo,{'OorNum','Dat'});

% find cows in 'hok' 72,92 and 61 during July 1 to july 31
% not all cows participate, but no indication of hok 60 is available
ind = find((cowinfo.Hok == 92 | cowinfo.Hok == 61 | cowinfo.Hok == 72) ...
           & (datenum(cowinfo.Dat) > datenum(2019,6,30) & ...
              datenum(cowinfo.Dat) < datenum(2019,8,1)));
cowids = unique(cowinfo.OorNum(ind));

% select cowsinfo of cows that are in cowids
cowinfo = cowinfo(ismember(cowinfo.OorNum,cowids)==1,:);

% add daily yield
cowinfo.DMY = cowinfo.KgMlkO + cowinfo.KgMlkA;

% visualise daily milkproduction
close all
h = figure('Units','centimeters','OuterPosition',[1 1 30 15]);
    hold on; box on;
    xlabel('Date');
    ylabel('Milk yield (kg/d)')
cols = F0_othercolor('Mrusttones',length(cowids));  % set colors
colsgr = F0_othercolor('Greys9',2*length(cowids));  % set colors
for i = 1:length(cowids)
   
    % find data from cowid in entire dataset
    ind = find(cowinfo.OorNum == cowids(i) & ~isnan(cowinfo.DMY) &...
               (datenum(cowinfo.Dat) > datenum(2019,5,1) & ...
                datenum(cowinfo.Dat) < datenum(2019,9,1)));
    plot([cowinfo.Dat(ind(1)) cowinfo.Dat(ind(1))],[0 60],'--','Color',cols(i,:),...
         'LineWidth',1)
     
    % plot the entire milking data per date
    plot(cowinfo.Dat(ind),cowinfo.DMY(ind),'o-','Color',colsgr(i,:),...
         'LineWidth',0.7,'MarkerSize',3)
    
    % find data from cowid in entire dataset
    ind = find(cowinfo.OorNum == cowids(i) & ~isnan(cowinfo.DMY) &...
               (datenum(cowinfo.Dat) > datenum(2019,6,30) & ...
                datenum(cowinfo.Dat) < datenum(2019,8,1)));
    
    % plot the milking data in july
    plot(cowinfo.Dat(ind),cowinfo.DMY(ind),':','Color',cols(i,:),...
         'LineWidth',1.5)

    
end
    
axis tight
title('Overview milk yield (kg/d) cows in tracklab trial')

% save figure
dirresults = ['C:\Users\adria036\OneDrive - Wageningen ' ...
           'University & Research\iAdriaens_doc\Projects\'...
           'eEllen\B4F_indTracking_cattle\B4F_results\'];
saveas(h,[dirresults 'FIG_DMYallcows.fig']);
saveas(h,[dirresults 'FIG_DMYallcows.png']);

% save dataset
writetable(cowinfo, [dataDir 'D_cowAncInfo.xlsx'])

% cows not pregnant
notpreg = unique(cowinfo.OorNum(isnat(cowinfo.VlgKlfDat) & ...
                                datenum(cowinfo.Dat) > datenum(2019,7,1),:));
% overlap in data (tracklab and non-pregnant): 267 316 534 1064 7715 8108


