## B4F_indTracking

    B4F individual tracking of dairy cattle.  
    Created on April, 15, 2021 by Ines Adriaens  
----------------------------
__Data__: UWB positioning data collected at Dairy Campus (Leeuwarden)  

__Objectives__:  
(1) data exploration and pattern detection;  
(2) analysis of spatial patterns;  
(3) analysis of (x,y,z) data in function of behavior.

-----------------------------
__S0__: transfer data to local directory  
__S1__: read data and accumulate for data reduction  
__S2__: load data for analysis  
__S3__: explore and visualize missing values  
__S4__: data exploration and averages 

__p\_tracklab__: functions to decode tracklab .tlp files  
__p\_read_tlp__: script to call tracklab decode functions and write to txt
