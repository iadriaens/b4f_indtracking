%% S7_Modelling
% this script starts from the 'complete' / imputed UWB data and implements
% the modelling and event detection. As the missing data imputation is run
% on HPC, the output files need to be combined in a single structure first.
% The final goal of this script is to obtain an idea of the 'standing up'
% and 'lying down', visualise these together with the annotated events.
% 
% INPUT =  data from HPC and annotated meta data
% OUTPUT = performance of models to a) detect lying behaviour
%                                   b) time lie down and get up
%                               ... additional analyses
%
%
% STEP 0: set filepaths, constants and load data
% STEP 1: general visualisation of z+dist together with annotations
% STEP 2: model development

clear variables
close all
clc


%% STEP 0: set and load

% set filepaths and filenames
init_.datadir = ['C:\Users\adria036\OneDrive - Wageningen ' ...
                'University & Research\iAdriaens_doc\Projects\'...
                'eEllen\B4F_indTracking_cattle\B4F_data\'];
init_.resdir = ['C:\Users\adria036\OneDrive - Wageningen '...
                'University & Research\iAdriaens_doc\Projects\' ...
                'eEllen\B4F_indTracking_cattle\B4F_results\'];

% load annotations
load([init_.datadir 'D0_meta_events.mat'])

% load and combine cow data
for i = 1:32
    % set filename
    init_.fn = sprintf('HPCdata_%d',i);
    
    % load data
    load([init_.datadir init_.fn])
    
    % set fieldnames
    C = sprintf('cow_%d',data.object_name(1));
    % combine data in cdata with fields
    cdata.(C) = data;
end

% clear variables
clear C i data datadir 

%% STEP 2: visualization z/dist smooth and annotations
% data_meta contains annotations
% two types of visualization 
%   - starting from all UWB data - one figure per day per cow
%   - starting from the annotated events - one figure per eventtype

% fields = cows
fields_ = fieldnames(cdata);

% UWB data per day
close all
for i = 1:length(fields_)
    data = cdata.(fields_{i});   % data from cow i
    days = unique(floor(datenum(data.date)));
    T=1;
    for ii = 1:length(days)
        % plot annotated events
        data_ann = data_meta.allses(data_meta.allses.cow == ...
                                data.object_name(1) & ...
                                floor(datenum(data_meta.allses.numstart))...
                                == days(ii),:);
        data_ann.time_in_h = (data_ann.numstart - ...
                              floor(data_ann.numstart)) .* 24;
                            
        if ~isempty(data_ann)  % if annotated data available
            
            % prepare figure
            h = figure('Units','centimeters','OuterPosition',[1 2 40 20]);
            yyaxis left 
            hold on; box on; ylim([-3 3.0])
            title(['Cow ' num2str(data.object_name(1)), ', date = ' ...
                   datestr(days(ii))]);
            xlabel('Time')
            ylabel('Data')

            % plot data and imputed data - zsmooth
            dataday = data(floor(datenum(data.date))==days(ii),:);
            plot(dataday.time_in_h, dataday.avg_z_sm,'LineWidth',0.8,...
                                                'Color',[205 92 92]./255);

            plot(dataday.time_in_h(~isnan(dataday.misscode)), ...
                 2*ones(1,sum(~isnan(dataday.misscode))),'s',...
                            'LineWidth',0.8,...
                            'MarkerSize',2,...
                            'Color',[92 92 92]./255);
            yyaxis right
            plot(dataday.time_in_h, dataday.dist_sm,'LineWidth',0.8,...
                                                'Color',[75 0 130]./255);
            ylim([-0.05 5])
            
            yyaxis left
                    
            % plot annotated data
            for iii = 1:height(data_ann)
                if data_ann.updown(iii) == 1 % down
                    plot([data_ann.time_in_h(iii) data_ann.time_in_h(iii)],...
                         [-5 5],'-','LineWidth',1.5,'Color',[60 179 113]./255)
                else % up
                    plot([data_ann.time_in_h(iii) data_ann.time_in_h(iii)],...
                         [-5 5],'-','LineWidth',1.5,'Color',[30 144 255]./255)
                     if iii > 1
                        data_ann.durlie(iii) = data_ann.starttime(iii) - ...
                                            data_ann.starttime(iii-1);
                     end
                end
            end
            xlim([min(data_ann.time_in_h)-0.5, max(data_ann.time_in_h)+0.5])
            
            T = T+1;
            if rem(T,5) == 0
                saveas(h,[init_.resdir 'Fig_lyingbouts_cow ' ...
                           num2str(data.object_name(1)) '_' ...
                           datestr(days(ii),'yyyymmdd')]);
                saveas(h,[init_.resdir 'Fig_lyingbouts_cow ' ...
                           num2str(data.object_name(1)) '_' ...
                           datestr(days(ii),'yyyymmdd') '.tif']);
            end 
        end
    end
end
close all

% clear variables
clear ans data data_ann dataday days h i ii iii T timerval



%% STEP3: statistical properties across getup/liedown
% preparation for modelling > summarize the statistical properties for
% getting up and lying down per cow and per day. This allows to investigate
% whether a model will be able to capture the individual variability.
%   1) Determine the episodes a cow is lying down according to the
%      IceQube data (start and end lying bouts). 
%   2) Add flag to UWB data (cdata)
%   3) Calculate statistical properties of the different parameters
%   4) Add 'distance from center' of the barn based on (x,y)
%   4) Compare statistical properties of the different variables and
%   visualize the potential factors affecting these properties

% add lying flag to cdata based on IceQube lying bouts
T=1; % counter
for i = 1:height(data_meta.IceQ)
    cow = sprintf('cow_%d',data_meta.IceQ.cow(i));    % cow = fieldname
    
    % start and end of lying bout
    boutstart = data_meta.IceQ.numtime(i);
    boutend = datenum(data_meta.IceQ.eDat(i))  ...
            + data_meta.IceQ.eTim(i);
    
    % add to cdata
    if isfield(cdata,cow)
        cdata.(cow).islying(datenum(cdata.(cow).date) >= boutstart & ...
               datenum(cdata.(cow).date) <= boutend) = 1;
        cdata.(cow).IceBout(datenum(cdata.(cow).date) >= boutstart & ...
               datenum(cdata.(cow).date) <= boutend) = T;
        data_meta.IceQ.IceBout(i) = T;
        T = T+1;
    end
end

% clear variables
clear boutend boutstart cow i

% scale for barn dimentions and calculate distance from center 
% distance to center assumed to be +/- constant when lying down

fields_ = fieldnames(cdata);
init_.center1 = [10.75,7];
init_.center2 = [10.75+21.5,7];

for i = 1:length(fields_)
    % delete measurements severely out of boundaries
    m = nanmean(cdata.(fields_{i}).avg_x_sm);  % <21.5 = c1  > 21.5 = 2
    
    % correct for sensors in feeding area (lost)
    cdata.(fields_{i}).avg_y_sm(cdata.(fields_{i}).avg_y_sm > 16) = NaN;
    % scale to fit in barn coordinates
    cdata.(fields_{i}).avg_y_sm = ...
        cdata.(fields_{i}).avg_y_sm .* 14 ...
        ./ max(cdata.(fields_{i}).avg_y_sm);
    
    % scale x smooth + add center distance (Euclidian)
    if m < 2*init_.center1(1)
        cdata.(fields_{i}).avg_x_sm(cdata.(fields_{i}).avg_x_sm < 0) = 0;
        cdata.(fields_{i}).avg_x_sm(cdata.(fields_{i}).avg_x_sm > 21.5) = 21.5;
        cdata.(fields_{i}).centerdist = sqrt((cdata.(fields_{i}).avg_x_sm ...
                                                - init_.center1(1)).^2 ...
                                            + (cdata.(fields_{i}).avg_y_sm ...
                                                - init_.center1(2)).^2);
    else
        cdata.(fields_{i}).avg_x_sm(cdata.(fields_{i}).avg_x_sm < 21.5) = 21.5;
        cdata.(fields_{i}).avg_x_sm(cdata.(fields_{i}).avg_x_sm > 43) = 43;
        cdata.(fields_{i}).centerdist = sqrt((cdata.(fields_{i}).avg_x_sm ...
                                                - init_.center2(1)).^2 ...
                                            + (cdata.(fields_{i}).avg_y_sm ...
                                                - init_.center2(2)).^2);
    end
    
    % flag when cows are not in lying area
    cdata.(fields_{i}).inslatted(cdata.(fields_{i}).avg_y_sm >= 10.5) = 1;    
end

% clear variables
clear m i ans

% summarize distributions of the different variables
%       a) per lying bout
%       b) per cow
% level, std, median, Q1, Q2,
%       + add % of missing data during a certain behaviour


% test fit distribution
for i = 17:22
    data = cdata.cow_90(cdata.cow_90.IceBout == i,:);
    h=figure; hold on
    histogram(data.avg_z_sm,'Normalization','pdf')
    test.fit = fitdist(data.avg_z_sm,'Normal');
    test.xval = 0:0.01:2.5;
    test.yval = pdf(test.fit,test.xval);
    plot(test.xval,test.yval,'LineWidth',2,'Color','r')
    test.fit2 = fitdist(data.avg_z_sm+0.01,'Lognormal');
    test.yval2 = pdf(test.fit2,test.xval);
    plot(test.xval,test.yval2,'LineWidth',2,'Color','m')
    test.fit3 = fitdist(data.avg_z_sm+0.01,'Loglogistic');
    test.yval3 = pdf(test.fit3,test.xval);
    plot(test.xval,test.yval3,'LineWidth',2,'Color','b')
    test.fit4 = fitdist(data.avg_z_sm+0.01,'Gamma');
    test.yval4 = pdf(test.fit4,test.xval);
    plot(test.xval,test.yval4,'LineWidth',2,'Color','g')
    legend({'z-data','Normal','Lognormal','Loglogistic','Gamma'})
    box on; xlabel('smoothed z');ylabel('pdf')
    title('Distributions during lying behaviour')
    saveas(h,[init_.resdir 'Fig_distr_IceQ_' num2str(i) '.tif'])
end


% add to data_meta.IceQ + make separate table for summary per animal
for i = 1:height(data_meta.IceQ)
    cow = sprintf('cow_%d',data_meta.IceQ.cow(i));
    try
        data = cdata.(cow)(cdata.(cow).IceBout == data_meta.IceQ.IceBout(i),:);
    catch
        data = [];
    end
    if ~isempty(data)
        % percentage missing
        data_meta.IceQ.Perc_miss(i,1) = sum(isnan(data.avg_z_sm)) ./ ...
                                            height(data)*100;
        % avg_z_sm
        data_meta.IceQ.Zavg(i,1) = nanmean(data.avg_z_sm);
        data_meta.IceQ.Zstd(i,1) = nanstd(data.avg_z_sm);
        data_meta.IceQ.Zmed(i,1) = nanmedian(data.avg_z_sm);
        data_meta.IceQ.ZQ1(i,1) = quantile(data.avg_z_sm,0.25);
        data_meta.IceQ.ZQ3(i,1) = quantile(data.avg_z_sm,0.75);
        data_meta.IceQ.Zskew(i,1) = skewness(data.avg_z_sm);

        % centerdist
        data_meta.IceQ.CDavg(i,1) = nanmean(data.centerdist);
        data_meta.IceQ.CDstd(i,1) = nanstd(data.centerdist);
        data_meta.IceQ.CDmed(i,1) = nanmedian(data.centerdist);
        data_meta.IceQ.CDQ1(i,1) = quantile(data.centerdist,0.25);
        data_meta.IceQ.CDQ3(i,1) = quantile(data.centerdist,0.75);
        data_meta.IceQ.CDskew(i,1) = skewness(data.centerdist);
        
    else   % fill with NaN
        data_meta.IceQ.Perc_miss(i,1) = NaN;
        % avg_z_sm
        data_meta.IceQ.Zavg(i,1) = NaN;
        data_meta.IceQ.Zstd(i,1) = NaN;
        data_meta.IceQ.Zmed(i,1) = NaN;
        data_meta.IceQ.ZQ1(i,1) = NaN;
        data_meta.IceQ.ZQ3(i,1) = NaN;
        data_meta.IceQ.Zskew(i,1) = NaN;

        % centerdist
        data_meta.IceQ.CDavg(i,1) = NaN;
        data_meta.IceQ.CDstd(i,1) = NaN;
        data_meta.IceQ.CDmed(i,1) = NaN;
        data_meta.IceQ.CDQ1(i,1) = NaN;
        data_meta.IceQ.CDQ3(i,1) = NaN;
        data_meta.IceQ.CDskew(i,1) = NaN;
    end
end

% select cdata ~cows for which IceQ is available AND from first to last
% lying bout, and select data_meta IceQ for which Cdata is available.
data_meta.IceLy = data_meta.IceQ(~isnan(data_meta.IceQ.Perc_miss),:);
%%
% remaining cows
cows = unique(data_meta.IceLy.cow);

% prepare summary
data_meta.cowliesum = table(cows,'VariableNames',{'cow'});

% select and summarize
for i = 1:length(cows)
    cow = sprintf('cow_%d',cows(i));
    
    % first lying bout of cow
    idxstart = find(data_meta.IceLy.cow == cows(i),1,'first');
    idxend = find(data_meta.IceLy.cow == cows(i),1,'last');
    timefirst = data_meta.IceLy.numtime(idxstart);
    timelast = data_meta.IceLy.numtime(idxend) + ...      % last bout
               data_meta.IceLy.durlydown(idxend)/(24*60); % duration
           
    %%%%%%-------------------REMARK DURATION BOUT-------------------%%%%%%%
    % the duration of the bout in the access-derived table variable "dLB"
    % does not agree with endtime - begintime of the bout. We chose to use
    % endtime - begintime as the reference duration.
    %%%%%%-------------------REMARK DURATION BOUT-------------------%%%%%%%
    
    % start of first lying bout
    sdata.(cow) = cdata.(cow)(datenum(cdata.(cow).date) > timefirst & ...
                              datenum(cdata.(cow).date) < timelast,:);
    
    % summarize data in lying or not in lying bout per cow
    data_meta.cowliesum.NoBouts(i) = length(unique(sdata.(cow).IceBout));
    data_meta.cowliesum.Timespan(i) = timelast-timefirst;
    data_meta.cowliesum.Lyingdur(i) = sum(sdata.(cow).islying) / ...
                                      (24*3600); % in d
	data_meta.cowliesum.Lyingdurday_h(i) = data_meta.cowliesum.Lyingdur(i) ...
                                         ./ data_meta.cowliesum.Timespan(i)*24; % in h
    data_meta.cowliesum.Lyingperc(i) = data_meta.cowliesum.Lyingdur(i) ...
                                         ./ data_meta.cowliesum.Timespan(i) *100; % in %
    data_meta.cowliesum.Lyingboutlength(i) = mean(data_meta.IceLy.durlydown(...
                                        data_meta.IceLy.cow == cows(i)));
    
    % summary lying (data) and not lying (up, data2)
    data = sdata.(cow)(sdata.(cow).islying == 1,:);
    data2 = sdata.(cow)(sdata.(cow).islying == 0,:);
   
    data_meta.cowliesum.Zavg_lying(i) = nanmean(data.avg_z_sm);
    data_meta.cowliesum.Zavg_up(i) = nanmean(data2.avg_z_sm);
    data_meta.cowliesum.Zstd_lying(i) = nanstd(data.avg_z_sm);
    data_meta.cowliesum.Zstd_up(i) = nanstd(data2.avg_z_sm);
    data_meta.cowliesum.Zmed_lying(i) = nanmedian(data.avg_z_sm);
    data_meta.cowliesum.Zmed_up(i) = nanmedian(data2.avg_z_sm);
    data_meta.cowliesum.ZQ1_lying(i) = quantile(data.avg_z_sm,0.25);
    data_meta.cowliesum.ZQ1_up(i) = quantile(data2.avg_z_sm,0.25);
    data_meta.cowliesum.ZQ3_lying(i) = quantile(data.avg_z_sm,0.75);
    data_meta.cowliesum.ZQ3_up(i) = quantile(data2.avg_z_sm,0.75);
    data_meta.cowliesum.Zmissing_lying(i) = sum(isnan(data.avg_z_sm)./ ...
                                            height(data))*100;
    data_meta.cowliesum.Zmissing_up(i) = sum(isnan(data2.avg_z_sm)./ ...
                                            height(data2))*100; 
    
	% center distance summary per cow
    data_meta.cowliesum.CDavg_lying(i) = nanmean(data.centerdist);
    data_meta.cowliesum.CDavg_up(i) = nanmean(data2.centerdist);
    data_meta.cowliesum.CDstd_lying(i) = nanstd(data.centerdist);
    data_meta.cowliesum.CDstd_up(i) = nanstd(data2.centerdist);
    data_meta.cowliesum.CDmed_lying(i) = nanmedian(data.centerdist);
    data_meta.cowliesum.CDmed_up(i) = nanmedian(data2.centerdist);
    data_meta.cowliesum.CDQ1_lying(i) = quantile(data.centerdist,0.25);
    data_meta.cowliesum.CDQ1_up(i) = quantile(data2.centerdist,0.25);
    data_meta.cowliesum.CDQ3_lying(i) = quantile(data.centerdist,0.75);
    data_meta.cowliesum.CDQ3_up(i) = quantile(data2.centerdist,0.75);
    
    % stats visualisation per animal incl. normal distribution
    h = figure('Units','centimeters','OuterPosition',[2 2 18 20]);
    subplot(2,1,1); hold on; box on; xlim([0 2.5]); ylim([0 2])
    histogram(data.avg_z_sm,'Normalization','pdf')
    test.fit = fitdist(data.avg_z_sm,'Normal');
    test.xval = 0:0.01:2.5;
    test.yval = pdf(test.fit,test.xval);
    plot(test.xval,test.yval,'LineWidth',2,'Color','r')
    legend({'z-data','Normal'})
    box on; xlabel('smoothed z');ylabel('pdf')
    title(['Distributions during lying behaviour, cow = ' num2str(cows(i))])
    
    subplot(2,1,2); hold on; box on; xlim([0 2.5]); ylim([0 2])
    histogram(data2.avg_z_sm,'Normalization','pdf')
    test.fit = fitdist(data2.avg_z_sm,'Normal');
    test.xval = 0:0.01:2.5;
    test.yval = pdf(test.fit,test.xval);
    plot(test.xval,test.yval,'LineWidth',2,'Color','r')
    legend({'z-data','Normal'})
    box on; xlabel('smoothed z');ylabel('pdf')
    title(['Distributions during non-lying behaviour, cow = ' num2str(cows(i))])
    
    % save figure
    saveas(h,[init_.resdir 'Fig_distr_lying_up_' num2str(cows(i)) '.tif'])
    
end


% clear variables
clear  i idxend idxstart T test timefirst timelast h fields_ data data2 cow cows ans

% % save variables
save([init_.resdir 'D2_data_meta.mat'],'data_meta')
save([init_.resdir 'D3_sdata_IQ_UWB.mat'],'sdata')

close all
clc














